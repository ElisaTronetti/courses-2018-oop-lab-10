package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new Song(songName, albumName, duration));
    }

    //prendo la canzone e per ogni canzone gli richiedo il suo nome e lo ordino in modo naturale (quindi dalla A alla Z)
    @Override
    public Stream<String> orderedSongNames() {
        return this.songs.stream().map(Song::getSongName).sorted();
    }

    //prendo la mappa degli album, prendo solo le chiavi (che contengono la stringa con il nome dell'album) e uso gli stream
    @Override
    public Stream<String> albumNames() {
        return this.albums.keySet().stream();
    }

    //prendo la mappa degli album, prendo i valori (l'anno dell'album) e con gli stream li filtro tutti, lasciando solo quelli con l'anno uguale a quello richiesto
    //e per ognuno richiedo la sua chiave (ovvero il nome dell'album)
    @Override
    public Stream<String> albumInYear(final int year) {
        return this.albums.entrySet().stream().filter(e -> e.getValue() == year).map(e -> e.getKey());
    }

    //per contare le canzoni ho predo il set delle canzoni, ho filtrato solo quelle che appartengono a un album, per poi filtrare quelle
    //che hanno il nome dell'album uguale a quello richiesto e con count le conto tutte
    //casto a int perché il valore di ritorno di count è double
    @Override
    public int countSongs(final String albumName) {
        return (int) this.songs.stream().filter(i -> i.getAlbumName().isPresent()).filter(i -> i.getAlbumName().get().equals(albumName)).count();
    }

    //più o meno allo stesso modo prendo il set delle canzoni e filtro solo quelle che non appartengono a un album per poi contarle
    @Override
    public int countSongsInNoAlbum() {
        return (int) this.songs.stream().filter(i -> !i.getAlbumName().isPresent()).count();
    }

    //prendo il set delle canzoni e per ognuna filtro solo quelle che appartengono a un album, per poi prendere solo quelle che appartengono all'album
    //specificato in input e poi creo una mappa con il nome della canzone e la durata della canzone e infine calcolo la media della durata della canzone
    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return this.songs.stream().filter(i -> i.getAlbumName().isPresent()).filter(i -> i.getAlbumName().get().equals(albumName)).mapToDouble(i -> i.getDuration()).average();
    }

    //prendo il set delle canzoni e creo una collezione che prende il massimo tra a e b (in base alla durata) e una volta trovato il massimo prendo il nome della canzone
    @Override
    public Optional<String> longestSong() {
        return this.songs.stream()
                .collect(Collectors.maxBy((a, b) -> Double.compare(a.getDuration(), b.getDuration())))
                .map(Song::getSongName);
    }

    //prendo il set delle canzoni, filtro solo le canzoni che appartengono a un album, creo una collection che raggruppa il nome dell'album con la somma della durata
    //delle canzoni per poi ritornare la chiave che appartiene all'album con la durata delle canzoni più lunga
    @Override
    public Optional<String> longestAlbum() {
        return this.songs.stream().filter(a -> a.getAlbumName().isPresent())
                .collect(Collectors.groupingBy(Song::getAlbumName, Collectors.summingDouble(Song::getDuration)))
                .entrySet().stream()
                .collect(Collectors.maxBy((e1, e2) -> Double.compare(e1.getValue(), e2.getValue())))
                .flatMap(Entry::getKey);
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }
    }
}
